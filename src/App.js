import React, { useEffect, useState } from 'react'
import './styles/main.scss'
import axios from 'axios'
import { Icon } from './components/icon'
import { useFirebase, useCol, useDoc } from './hooks/firebase.js'
import { PopUp, Charts, Box, Button, Text, WeatherIcon } from './components'

const MonthConverter = (month) => {
  switch (month) {
    case 1:
      return 'January'
    case 2:
      return 'February'
    case 3:
      return 'March'
    case 4:
      return 'April'
    case 5:
      return 'May'
    case 6:
      return 'June'
    case 7:
      return 'July'
    case 8:
      return 'August'
    case 9:
      return 'September'
    case 10:
      return 'October'
    case 11:
      return 'November'
    case 12:
      return 'December'
    default:
  }
}

const DateConverter = (date, time) => {
  let data = date.split('-')
  let year = data[0]
  let month = Number(data[1])
  let day = data[2]
  time = (Number(time.slice(0, 2)) + 8) + time.slice(2, 6);

  return (MonthConverter(month) + ' ' + day + ', ' + year + ' ' + time);
}

const RandomNumberAndString = () => {
  let result = [];
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let charactersLength = characters.length;
  for (let i = 0; i < 15; i++) {
    result.push(characters.charAt(Math.floor(Math.random() *
      charactersLength)));
  }
  return result.join('');
}

const GetDayNames = (date) => {
  const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const d = new Date(date)
  return days[d.getDay()]
}


const App = () => {
  // Firebase
  const { auth, firestore, firebase } = useFirebase();
  // Authintcation
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [user, setUser] = useState(null);
  const [userVerified, setUserVerified] = useState('default');
  const [login, setLogin] = useState(true);
  // Weather
  const [weatherData, setWeatherData] = useState(null);
  const [temperature, setTemperature] = useState('')
  const [main, setMain] = useState('')
  // Forecast Weather
  const [timeWeatherData, setTimeWeatherData] = useState([]);
  const [dayWeatherData, setDayWeatherData] = useState([]);
  // Errors
  const [error, setError] = useState('')
  const [loginError, setLoginError] = useState('')
  const [activityError, setActivityError] = useState('')
  // Data's
  const [activityScore, setActivityScore] = useState(1)
  const [chartData, setChartData] = useState([]);
  // Firestore Data's
  const { data, createRecord } = useCol('/Users/')
  const { data: feedBackData } = useCol('/FeedBack/')
  const { data: lastActivity, updateRecord: updateLastActivity, readAgain } = useDoc(`/Users/${user?.uid}`)
  const { data: activityScores, createRecord: createActivity } = useCol(`/Users/${user?.uid}/ActivityScores/`)
  // date, times
  const date = new Date().toISOString().slice(0, 10);
  const time = new Date().toISOString().slice(11, 16);
  // feedback
  const [feedback, setFeedback] = useState(false);
  // window width, height
  const height = window.innerHeight
  const width = window.innerWidth
  // Theme 
  const [theme, setTheme] = useState(null)

  // API Request
  useEffect(() => {
    axios.get(`https://api.weatherbit.io/v2.0/current?city=Ulaanbaatar&key=0ef618d020a843c993e4adc0b0d46b78`)
      .then(res => {
        const data = res.data
        setWeatherData(data);
        setTemperature(data.data[0].temp);
        setMain(data?.data[0].weather.description);
        setTheme(data?.data[0].weather.icon[3])
      }).catch((e) => {
        alert(e.message)
      })
  }, [])

  useEffect(() => {
    axios.get(`https://api.weatherbit.io/v2.0/forecast/hourly?city=Ulaanbaatar&key=0ef618d020a843c993e4adc0b0d46b78&hours=24`)
      .then(res => {
        const data = res.data
        data?.data.map((e, i) => {
          const weatherDate = e.timestamp_local.toString()
          if (weatherDate.slice(0, 10) === date) {
            let time = '';
            if ((Number(e.timestamp_local.slice(11, 13))) >= 24) {
              time = '0' + (Number(e.timestamp_local.slice(11, 13)) - 24) + ' AM'
            } else {
              time = (Number(e.timestamp_local.slice(11, 13))) + " PM"
            }
            setTimeWeatherData(timeWeatherData => {
              return [
                {
                  date: time,
                  temp: e.temp,
                  id: i,
                  icon: e?.weather.icon,
                  main: e?.weather.description
                }, ...timeWeatherData]
            })
          }
        })
      }).catch((e) => {
        alert(e.message)
      })
  }, [])

  useEffect(() => {
    axios.get(`https://api.weatherbit.io/v2.0/forecast/daily?city=Ulaanbaatar&key=0ef618d020a843c993e4adc0b0d46b78`)
      .then(res => {
        const data = res.data
        data.data.map((e, i) => {
          if(i <= 5 && i != 0) {
            setDayWeatherData(dayWeatherData => {
              return [
                {
                  date: GetDayNames(e.valid_date).slice(0, 3),
                  temp: e.temp,
                  id: i,
                  icon: e?.weather.icon,
                  main: e?.weather.description
                }, ...dayWeatherData]
            })
          }
        })
      }).catch((e) => {
        alert(e.message)
      })
  }, [])
  
  // UseEffect
  useEffect(() => {
    if (activityScores) {
      firestore?.collection(`Users/${user?.uid}/ActivityScores`).orderBy('createdAt')
        .onSnapshot((snapshot) => {
          setChartData(snapshot.docs.map((e, i) => {
            const data = e.data();
            return {
              'name': data.date,
              'activityScore': Number(data.activityScore)
            }
          }))
        });
    }
  }, [activityScores])

  useEffect(() => {
    if (lastActivity?.feedback) {
      setFeedback(true)
    }
  }, [user?.uid, lastActivity])

  const sortById = (data) => {
    return data?.sort((a, b) => {
      return a.id - b.id;
    })
  }

  // Auth
  const SignUp = (email, password) => {
    setUserVerified(false)
    auth.createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        setUser(user);
        setUserVerified('verified');
        createRecord(user?.uid, {
          email: user.email,
          username: username,
          uid: user.uid
        });
        setError('')
        setLoginError('')
        setActivityError('')
      })
      .catch((error) => {
        setUserVerified('default')
        setError(error.message)
      });
  }

  const LogOut = () => {
    auth.signOut();
    setUserVerified('default');
    setError('');
    setLoginError('');
    setUser('');
  }

  const Login = (email, password) => {
    setUserVerified(false)
    auth.signInWithEmailAndPassword(email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        setUser(user)
        setUserVerified('verified')
        setError('')
        setLoginError('')
        setActivityError('')
      })
      .catch((error) => {
        setUserVerified('default')
        setLoginError(error.message)
      });
  }

  // DB
  const GetUsername = (data, uid) => {
    let username = null;
    data.map((e) => {
      if (e.uid === uid) {
        username = e.username;
        return e.username;
      }
    })
    if (username) {
      return username;
    }
  }

  const UpdateActivity = async () => {
    if ((user?.uid && lastActivity) && Number(activityScore) != 0) {
      const id = RandomNumberAndString();
      let tmp = new Date().toISOString().slice(0, 10).split('-').reverse().join('/');
      tmp = tmp.split('/');
      const date = tmp[1] + '/' + tmp[0] + '/' + tmp[2]

      const date1 = new Date(lastActivity.lastActivity);
      const date2 = new Date(date);
      const diffTime = Math.abs(date2 - date1);
      let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

      if (!lastActivity.lastActivity) {
        diffDays = 1
      }

      if (diffDays >= 1) {
        await createActivity(id, {
          activityId: id,
          activityScore: activityScore,
          date: date,
          createdAt: firebase.firestore.FieldValue.serverTimestamp()
        });
        await updateLastActivity({
          lastActivity: date
        })
        await setActivityError('Your Activity has been sent');
        readAgain()
      } else {
        setActivityError('You are cooldowns please send tomorrow')
      }
    } else {
      if (Number(activityScore) == 0) {
        setActivityError('Activity Score is invalid ..')
      } else {
        setActivityError('Please login ..')
      }
    }
  }

  // Loading ...
  if (!weatherData || !feedBackData) {
    return (
      <div className={'loader-box'} >
        <div className="lds-facebook">
          <div>
          </div>
          <div>
          </div>
          <div>
          </div>
        </div>
      </div>
    )
  }

  return (
    <Box >
      <div className={'flex-row'} style={{height: '130vh'}} >
        <div className={`${theme === 'n' ? 'night-container' : 'container'}`} >
          <Box className={'blur'} >
            <div className={'w100 h50 flex-row justify-between'} >
              <div className={'flex-col justify-between pa-20 h50'} >
                <div className={'w50 h100 flex-col justify-start items-start ml-10'} >
                  <div>
                    <Text theme={theme} className={`fs-40 test1`} >Ulaanbaatar</Text>
                    <Text className={'op fs-20'} >{DateConverter(date, time)} {GetDayNames(date)}</Text>
                  </div>
                  <div className={'flex-row items-center'} >
                    <Icon id={weatherData?.data[0].weather.icon} />
                    <Text theme={theme} className={`fs-40 rm `}>{main}</Text>
                  </div>
                </div>
                <div className={'w50 h-300 mt-40 textcenter flex-center'} >
                  <Text theme={theme} className={`fs-80 rm mt-40`} >{temperature}°</Text>
                </div>
              </div>
              <div className={'w50 h100 flex-col justify-between items-center'} >
                {
                  sortById(dayWeatherData).map((e, i) => {
                    return (
                      <div className={`h18 w98 flex-row items-center justify-around br-${theme == 'd' ? 'black': 'white'}-1 bradius-5`} key={i} >
                        <Text theme={theme} type={'s'}>{e.date}</Text>
                        <Text theme={theme} type={'s'}>{e.temp}°</Text>
                        <div className={'flex-row items-center'} >
                          <Icon id={e.icon} width={50} height={50} />
                          <Text theme={theme} type={'s'} className={'ml-10'} >{e.main}</Text>
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            </div>
            <div className={'flex-row'} style={{ height: ((height * 0.4) - 40) + 'px' }} >
              <div className={'w100 h100 flex-wrap flex justify-between'} >
                {
                  sortById(timeWeatherData).map((e, i) => {
                    return (
                      <div className={`w23 br-${theme == 'n' ? 'white' : 'black'}-1 pa-5 flex-center bradius-5 mt-5`} key={i} style={{ height: '40%' }} >
                        <div className={'w100 flex-row justify-between items-center'} >
                          <div className={'w100 flex-row items-center justify-start'} >
                            <Icon id={e.icon} width={40} height={40} />
                            <Text theme={theme} className={`rm fs-18 `} type={'s'}>{e.main}</Text>
                          </div>
                          <Text theme={theme} className={`rm fs-18 w40 `} type={'s'}>{e.date}</Text>
                        </div>
                        <div className={'flex-row'} >
                          <Text theme={theme} className={`rm fs-18 `} type={'s'}>{e.temp}°</Text>
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          </Box>
        </div>
        <div className={'w20 flex-center pt-10 br-black-1'} >
          {
            userVerified === 'verified' || !userVerified ?
              userVerified === 'verified' ?
                <>
                  <div>{GetUsername(data, user?.uid)}</div>
                  <Text className={'c-red fs-15 ul'} onClick={LogOut} >Logout</Text>
                </>
                :
                <>
                  <div class="lds-facebook">
                    <div>
                    </div>
                    <div>
                    </div>
                    <div>
                    </div>
                  </div>
                </>
              :
              login ?
                <>
                  <div className={"mb-20 pa-10 bradius-5 flex-row items-center"} >
                    <WeatherIcon width={30} height={30} />
                    <Text className={'ml-10'}>Login</Text>
                  </div>
                  <div className={'h80 flex-col items-center w100'} >
                    <div className={'flex-start w70'} >
                      <Text className={'fs-15 mb-10 op'} type={'s'} >Email</Text>
                      <input placeholder={'Email'} value={email} onChange={(e) => { setEmail(e.target.value) }} />
                    </div>
                    <div className={'flex-start w70 op'} >
                      <Text className={'fs-15'} type={'s'} >Password</Text>
                      <input placeholder={'Password'} type={'password'} value={password} onChange={(e) => { setPassword(e.target.value) }} />
                    </div>
                    <Button className={'w50 h-30 mb-15'} onClick={() => { Login(email, password) }} >Login</Button>
                    {
                      loginError ?
                        <Text className={'fs-10 rm mt-5 c-red ul textcenter h-10'} >{loginError}</Text>
                        :
                        user ?
                          <Text className={'fs-10 rm mt-5 c-green ul textcenter h-10'} >Verified, please wait</Text>
                          :
                          <div className={'h-19 w-10'} ></div>
                    }
                    <Text className={'fs-15 c-rblue ul mt-20'} onClick={() => { setLogin(false) }} >SignUp</Text>
                  </div>
                </>
                :
                <>
                  <div className={"mb-20 pa-10 bradius-5 flex-row items-center"} >
                    <WeatherIcon width={30} height={30} />
                    <Text className={'ml-10'}>SignUp</Text>
                  </div>
                  <div className={'flex-col h80 w100 items-center'} >
                    <div className={'flex-start w70'} >
                      <Text className={'fs-15 mb-10 op'} type={'s'} >Username</Text>
                      <input placeholder={'username'} value={username} onChange={(e) => { setUsername(e.target.value) }} />
                    </div>
                    <div className={'flex-start w70'} >
                      <Text className={'fs-15 mb-10 op'} type={'s'} >Email</Text>
                      <input placeholder={'email'} value={email} onChange={(e) => { setEmail(e.target.value) }} />
                    </div>
                    <div className={'flex-start w70'} >
                      <Text className={'fs-15 mb-10 op'} type={'s'} >Password</Text>
                      <input placeholder={'password'} type={'password'} value={password} onChange={(e) => { setPassword(e.target.value) }} />
                    </div>
                    <Button className={'w50 h-30 mb-15'} onClick={() => { SignUp(email, password) }} >SignUp</Button>
                    {
                      error ?
                        <Text className={'fs-10 rm mt-5 c-red ul textcenter h-10'} >{error}</Text>
                        :
                        <div className={'h-19 w-10'} ></div>
                    }
                    <Text className={'fs-15 c-rblue ul'} onClick={() => { setLogin(true) }} >Login</Text>
                  </div>
                </>
          }
        </div>
      </div>
      <Box className={'flex-row'} >
        <div className={'pa-8 flex-col items-center w30 br-black-1'} style={{ height: ((height - 358) + 'px') }} >
          <Text className={'rm mt-5'} >Today's activity</Text>
          <div className={'w100 h70 flex-col items-center justify-around'} >
            <Text className={'fs-15'} >{'Activity 0 -> 10'}</Text>
            <input className={'w50 rm'} type="number" min={'1'} max={'10'} value={activityScore} onChange={(e) => {
              if (e.target.value != '-' && e.target.value != '+') {
                if (Number(e.target.value) || e.target.value == '') {
                  if (e.target.value <= 10 && e.target.value >= 0)
                    setActivityScore(e.target.value)
                }
              }
            }} />
            {
              activityError === 'Your Activity has been sent' ?
                <Text className={'fs-10 rm mt-5 c-green ul textcenter h-10'} >{activityError}</Text>
                :
                activityError === 'You are cooldowns please send tomorrow' ?
                  <Text className={'fs-10 rm mt-5 c-red ul textcenter h-10'} >{activityError}</Text>
                  :
                  activityError ?
                    <Text className={'fs-10 rm mt-5 c-red ul textcenter h-10'}>{activityError}</Text>
                    :
                    <div className={'h-19 w-10'} ></div>
            }
            <Button className={'h-40 w-200'} onClick={UpdateActivity} >Send</Button>
          </div>
          {
            feedback != true && (
              user?.uid ?
                <PopUp uid={user?.uid} />
                :
                <Text className={'fs-10 rm mt-5 c-red ul textcenter h-10'}>Please login to send feedback !</Text>
            )
          }
        </div>
        <div className={'flex w70 br-black-1 justify-center items-center'} style={{ height: ((height - 342) + 'px') }}  >
          {
            user?.uid ?
              <Charts height={height - 342} width={width * 0.65} data={chartData} />
              :
              <p>Please Login to see your Activity</p>
          }
        </div>
      </Box>
    </Box>
  );
}

export default App;