import React, { useState } from 'react'
import { useCol, useDoc } from '../hooks/firebase.js'
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import { StarIcon } from './icons'

export const PopUp = ({ uid }) => {
    const [star, setStar] = useState(1);
    const { createRecord: sendFeedback } = useCol(`/FeedBack/`)
    const { updateRecord: sendFeedbackData } = useDoc(`/Users/${uid}/`)

    const Send = async () => {
        await sendFeedback(uid, {
          userId: uid,
          feedBack: star
        });
        await sendFeedbackData({
            feedback: true
          });
      }

    return (
        <Popup
            trigger={<button className={'h-40 w-150 fs-15 c-white b-positive bradius-5 rb'} >FeedBack !!</button>}
            modal
            nested
        >
            {
                close => (
                    <div className="modal">
                        <button className="close" onClick={close} >
                            &times;
                        </button>
                        <div className="header"> Feed Back !! </div>
                        <div className='flex justify-center w100' >
                            <div className="content flex justify-between">
                                <div onClick={() => { setStar(1) }} ><StarIcon active={star >= 1 ? true : false} /></div>
                                <div onClick={() => { setStar(2) }} ><StarIcon active={star >= 2 ? true : false} /></div>
                                <div onClick={() => { setStar(3) }} ><StarIcon active={star >= 3 ? true : false} /></div>
                                <div onClick={() => { setStar(4) }} ><StarIcon active={star >= 4 ? true : false} /></div>
                                <div onClick={() => { setStar(5) }} ><StarIcon active={star >= 5 ? true : false} /></div>
                            </div>
                        </div>
                        <div className="actions">
                            <button
                                className="button h-40 w-150 fs-20 c-white b-positive bradius-5 rb"
                                onClick={() => {
                                    Send()
                                    close()
                                }}
                            >
                                Submit
                  </button>
                        </div>
                    </div>
                )}
        </Popup>
    )
}