import React from 'react';

export const Text = ({ children, type = 'l', className = '', theme = '', ...others }) => {
    if (type == 's') {
        return (
            <p className={`text ${theme == 'n' && 'c-white'} ${className}`} {...others}>
                {children}
            </p>
        );
    }
    return (
        <h2 className={`text ${theme == 'n' && 'c-white'} ${className}`} {...others}>
            {children}
        </h2>
    );
};