import React from 'react'

export const Icon = ({id, width, height}) => {

    return (
        <div>
            <img width={width} height={height} src={`https://www.weatherbit.io/static/img/icons/${id}.png`} alt={'img'} />
        </div>
    )
}
